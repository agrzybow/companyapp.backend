INSERT INTO `UserPermission`
(id, mobileApp, consoleDisplay, consoleEdit, consoleAdd, modifyDate)
VALUES
(1, 1, 1, 1, 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `UserPermission`
(id, mobileApp, consoleDisplay, consoleEdit, consoleAdd, modifyDate)
VALUES
(2, 1, 1, 0, 0, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `UserGroup`
(id, groupText, modifyDate)
VALUES
(1, "Administrator", UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `UserGroup`
(id, groupText, modifyDate)
VALUES
(2, "Użytkownik", UNIX_TIMESTAMP(current_timestamp()));


INSERT INTO `User`
(username, password, salt, name, email, phoneNumber, userPermission, userGroup, passwordExpiredAt, modifyDate)
VALUES
('admin', '$2y$12$cQxzXqjPaWf4BSK8GdbJ4eJdBpFM26IcGIrbHaS79J8HJU3WTiduS', '', 'Administrator', 'admin@agrzybow.pl', '123 456 789', 1, 1, 100, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `User`
(username, password, salt, name, email, phoneNumber, userPermission, userGroup, passwordExpiredAt, modifyDate)
VALUES
('user', '$2y$12$cQxzXqjPaWf4BSK8GdbJ4eJdBpFM26IcGIrbHaS79J8HJU3WTiduS', '', 'Użytkownik Użytkownikowy', 'user@agrzybow.pl', '123 456 789', 2, 2, 100, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizCategory`
(id, categoryText, modifyDate)
VALUES
(1, 'Kategoria 1', UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizQuestion`
(id, questionText, hint, category, modifyDate)
VALUES
(1, 'Pytanie 1', 'Podpowiedz 1', 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizQuestion`
(id, questionText, hint, category, modifyDate)
VALUES
(2, 'Pytanie 2', 'Podpowiedz 2', 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(1, 'Odp1', true, 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(2, 'Odp2', false, 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(3, 'Odp3', false, 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(4, 'Odp4', false, 1, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(5, 'Odp5', true, 2, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(6, 'Odp6', false, 2, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(7, 'Odp7', false, 2, UNIX_TIMESTAMP(current_timestamp()));

INSERT INTO `QuizAnswer`
(id, answerText, isCorrect, question, modifyDate)
VALUES
(8, 'Odp8', false, 2, UNIX_TIMESTAMP(current_timestamp()));
