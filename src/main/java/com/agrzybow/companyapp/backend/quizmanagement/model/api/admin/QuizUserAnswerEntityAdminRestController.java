package com.agrzybow.companyapp.backend.quizmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizUserAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizUserAnswerEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/quiz-user-answer")
public class QuizUserAnswerEntityAdminRestController implements AdminRestControllerInterface<QuizUserAnswerEntity, Integer> {
    private QuizUserAnswerEntityRepository quizUserAnswerEntityRepository;

    @Autowired
    public QuizUserAnswerEntityAdminRestController(QuizUserAnswerEntityRepository quizUserAnswerEntityRepository) {
        this.quizUserAnswerEntityRepository = quizUserAnswerEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<QuizUserAnswerEntity> getAllEntities() {
        return quizUserAnswerEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public QuizUserAnswerEntity getEntityById(@PathVariable Integer id) {
        return quizUserAnswerEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<QuizUserAnswerEntity> createEntity(@RequestBody List<QuizUserAnswerEntity> newEntity) {
        return null;
    }

    @PostMapping("/edit")
    @Override
    public QuizUserAnswerEntity editEntity(@RequestBody QuizUserAnswerEntity editedEntity) {
        return null;
    }
}
