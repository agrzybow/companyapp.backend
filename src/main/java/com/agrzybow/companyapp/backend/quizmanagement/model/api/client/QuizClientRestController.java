package com.agrzybow.companyapp.backend.quizmanagement.model.api.client;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizCategoryEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizQuestionEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizUserAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizDTO;
import com.agrzybow.companyapp.backend.quizmanagement.model.dto.QuizUserAnswerDTO;
import com.agrzybow.companyapp.backend.quizmanagement.model.mapper.QuizAnswerEntityDTOMapper;
import com.agrzybow.companyapp.backend.quizmanagement.model.mapper.QuizCategoryEntityDTOMapper;
import com.agrzybow.companyapp.backend.quizmanagement.model.mapper.QuizQuestionEntityDTOMapper;
import com.agrzybow.companyapp.backend.quizmanagement.model.mapper.QuizUserAnswerEntityDTOMapper;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizAnswerEntityRepository;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizCategoryEntityRepository;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizQuestionEntityRepository;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizUserAnswerEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/client/quiz")
public class QuizClientRestController {
    private QuizUserAnswerEntityRepository quizUserAnswerEntityRepository;
    private QuizCategoryEntityRepository quizCategoryEntityRepository;
    private QuizQuestionEntityRepository quizQuestionEntityRepository;
    private QuizAnswerEntityRepository quizAnswerEntityRepository;

    private QuizUserAnswerEntityDTOMapper quizUserAnswerEntityDTOMapper;
    private QuizCategoryEntityDTOMapper quizCategoryEntityDTOMapper;
    private QuizQuestionEntityDTOMapper quizQuestionEntityDTOMapper;
    private QuizAnswerEntityDTOMapper quizAnswerEntityDTOMapper;

    @Autowired
    public QuizClientRestController(QuizUserAnswerEntityRepository quizUserAnswerEntityRepository,
                                    QuizCategoryEntityRepository quizCategoryEntityRepository,
                                    QuizQuestionEntityRepository quizQuestionEntityRepository,
                                    QuizAnswerEntityRepository quizAnswerEntityRepository,
                                    QuizUserAnswerEntityDTOMapper quizUserAnswerEntityDTOMapper,
                                    QuizCategoryEntityDTOMapper quizCategoryEntityDTOMapper,
                                    QuizQuestionEntityDTOMapper quizQuestionEntityDTOMapper,
                                    QuizAnswerEntityDTOMapper quizAnswerEntityDTOMapper) {
        this.quizUserAnswerEntityRepository = quizUserAnswerEntityRepository;
        this.quizCategoryEntityRepository = quizCategoryEntityRepository;
        this.quizQuestionEntityRepository = quizQuestionEntityRepository;
        this.quizAnswerEntityRepository = quizAnswerEntityRepository;
        this.quizUserAnswerEntityDTOMapper = quizUserAnswerEntityDTOMapper;
        this.quizCategoryEntityDTOMapper = quizCategoryEntityDTOMapper;
        this.quizQuestionEntityDTOMapper = quizQuestionEntityDTOMapper;
        this.quizAnswerEntityDTOMapper = quizAnswerEntityDTOMapper;
    }

    @PreAuthorize("#id == authentication.name")
    @GetMapping("/display/{id}")
    public List<QuizDTO> getQuizForUser(@PathVariable String id) {
        QuizUserAnswerEntity quizUserAnswerEntity = quizUserAnswerEntityRepository.findUserQuizAnswerInLast24Hours(UserEntity.builder().username(id).build(), System.currentTimeMillis()).orElse(null);
        if (quizUserAnswerEntity != null) {
            return new ArrayList<>();
        } else {
            List<QuizDTO> result = new ArrayList<>();

            List<QuizCategoryEntity> categories = quizCategoryEntityRepository.findAll();
            categories.forEach(quizCategoryEntity -> {
                QuizDTO.QuizDTOBuilder builder = QuizDTO.builder();
                builder.category(quizCategoryEntityDTOMapper.entityToDto(quizCategoryEntity));
                quizQuestionEntityRepository.findByCategory(quizCategoryEntity).ifPresent(questionEntityList -> {
                    QuizQuestionEntity questionEntity = questionEntityList.get(new Random().nextInt(questionEntityList.size()));
                    builder.question(quizQuestionEntityDTOMapper.entityToDto(questionEntity));
                    quizAnswerEntityRepository.findByQuestion(questionEntity).ifPresent(e -> builder.answers(quizAnswerEntityDTOMapper.entitiesToDtos(e)));
                });
                result.add(builder.build());
            });
            return result;
        }
    }

    @PreAuthorize("#id == authentication.name")
    @PostMapping("/user-answer/{id}")
    public QuizUserAnswerDTO saveQuizUserAnswer(@PathVariable String id, @RequestBody QuizUserAnswerDTO quizUserAnswerDTO) {
        return quizUserAnswerEntityDTOMapper.entityToDto(quizUserAnswerEntityRepository.save(quizUserAnswerEntityDTOMapper.dtoToEntity(quizUserAnswerDTO)));
    }
}
