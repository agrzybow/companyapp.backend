package com.agrzybow.companyapp.backend.quizmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizCategoryEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizCategoryEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/quiz-category")
public class QuizCategoryEntityAdminRestController implements AdminRestControllerInterface<QuizCategoryEntity, Integer> {
    private QuizCategoryEntityRepository quizCategoryEntityRepository;

    @Autowired
    public QuizCategoryEntityAdminRestController(QuizCategoryEntityRepository quizCategoryEntityRepository) {
        this.quizCategoryEntityRepository = quizCategoryEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<QuizCategoryEntity> getAllEntities() {
        return quizCategoryEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public QuizCategoryEntity getEntityById(@PathVariable Integer id) {
        return quizCategoryEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<QuizCategoryEntity> createEntity(@RequestBody List<QuizCategoryEntity> newEntity) {
        return quizCategoryEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public QuizCategoryEntity editEntity(@RequestBody QuizCategoryEntity editedEntity) {
        return quizCategoryEntityRepository.save(editedEntity);
    }
}
