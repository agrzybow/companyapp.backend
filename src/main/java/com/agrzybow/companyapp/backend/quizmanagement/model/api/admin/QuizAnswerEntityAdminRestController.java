package com.agrzybow.companyapp.backend.quizmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizAnswerEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.repository.QuizAnswerEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/quiz-answer")
public class QuizAnswerEntityAdminRestController implements AdminRestControllerInterface<QuizAnswerEntity, Integer> {
    private QuizAnswerEntityRepository quizAnswerEntityRepository;

    @Autowired
    public QuizAnswerEntityAdminRestController(QuizAnswerEntityRepository quizAnswerEntityRepository) {
        this.quizAnswerEntityRepository = quizAnswerEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<QuizAnswerEntity> getAllEntities() {
        return quizAnswerEntityRepository.findAll();
    }

    @GetMapping("/display{id}")
    @Override
    public QuizAnswerEntity getEntityById(@PathVariable Integer id) {
        return quizAnswerEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<QuizAnswerEntity> createEntity(@RequestBody List<QuizAnswerEntity> newEntity) {
        return quizAnswerEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public QuizAnswerEntity editEntity(@RequestBody QuizAnswerEntity editedEntity) {
        return quizAnswerEntityRepository.save(editedEntity);
    }
}
