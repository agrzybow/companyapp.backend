package com.agrzybow.companyapp.backend.quizmanagement.model.dto;

import lombok.Data;

@Data
public class QuizAnswerDTO {
    private Integer id;
    private String answerText;
    private Boolean isCorrect;
}
