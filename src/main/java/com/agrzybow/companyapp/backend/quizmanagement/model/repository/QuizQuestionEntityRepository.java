package com.agrzybow.companyapp.backend.quizmanagement.model.repository;

import com.agrzybow.companyapp.backend.quizmanagement.model.QuizCategoryEntity;
import com.agrzybow.companyapp.backend.quizmanagement.model.QuizQuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface QuizQuestionEntityRepository extends JpaRepository<QuizQuestionEntity, Integer> {
    Optional<List<QuizQuestionEntity>> findByCategory(QuizCategoryEntity category);
}
