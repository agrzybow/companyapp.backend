package com.agrzybow.companyapp.backend.mapmanagement.model.mapper;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.backend.mapmanagement.model.dto.MapSpotDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = MapSpotCategoryEntityDTOMapper.class)
public interface MapSpotEntityDTOMapper {
    MapSpotEntity dtoToEntity(MapSpotDTO mapSpotDTO);
    MapSpotDTO entityToDto(MapSpotEntity mapSpotEntity);
    List<MapSpotDTO> entitiesToDtos(List<MapSpotEntity> mapSpotEntities);
    List<MapSpotEntity> dtosToEntities(List<MapSpotDTO> mapSpotDTOs);
}
