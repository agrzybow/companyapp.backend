package com.agrzybow.companyapp.backend.mapmanagement.model.repository;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MapSpotEntityRepository extends JpaRepository<MapSpotEntity, Integer> {
    List<MapSpotEntity> findByModifyDateGreaterThan(Long modifyDate);
}
