package com.agrzybow.companyapp.backend.mapmanagement.model.api.admin;

import com.agrzybow.companyapp.backend.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.backend.mapmanagement.model.repository.MapSpotCategoryEntityRepository;
import com.agrzybow.companyapp.backend.general.model.api.admin.AdminRestControllerInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/map-spot-category")
public class MapSpotCategoryEntityAdminRestController implements AdminRestControllerInterface<MapSpotCategoryEntity, Integer> {
    private MapSpotCategoryEntityRepository mapSpotCategoryEntityRepository;

    @Autowired
    public MapSpotCategoryEntityAdminRestController(MapSpotCategoryEntityRepository mapSpotCategoryEntityRepository) {
        this.mapSpotCategoryEntityRepository = mapSpotCategoryEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<MapSpotCategoryEntity> getAllEntities() {
        return mapSpotCategoryEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public MapSpotCategoryEntity getEntityById(@PathVariable Integer id) {
        return mapSpotCategoryEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<MapSpotCategoryEntity> createEntity(@RequestBody List<MapSpotCategoryEntity> newEntity) {
        return mapSpotCategoryEntityRepository.saveAll(newEntity);
    }

    @PostMapping("/edit")
    @Override
    public MapSpotCategoryEntity editEntity(@RequestBody MapSpotCategoryEntity editedEntity) {
        return mapSpotCategoryEntityRepository.save(editedEntity);
    }
}
