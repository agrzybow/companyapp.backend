package com.agrzybow.companyapp.backend.postmanagement.model.repository;

import com.agrzybow.companyapp.backend.postmanagement.model.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PostEntityRepository extends JpaRepository<PostEntity, Integer> {
    Optional<List<PostEntity>> findByModifyDateGreaterThan(Long newerThan);
}
