package com.agrzybow.companyapp.backend.postmanagement.model.api.client;

import com.agrzybow.companyapp.backend.postmanagement.model.dto.PostDTO;
import com.agrzybow.companyapp.backend.postmanagement.model.mapper.PostEntityDTOMapper;
import com.agrzybow.companyapp.backend.postmanagement.model.repository.PostEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/client/post")
public class PostEntityClientRestController {
    private PostEntityRepository postEntityRepository;
    private PostEntityDTOMapper mapper;

    @Autowired
    public PostEntityClientRestController(PostEntityRepository postEntityRepository, PostEntityDTOMapper mapper) {
        this.postEntityRepository = postEntityRepository;
        this.mapper = mapper;
    }

    @GetMapping("/display")
    public List<PostDTO> getAllEntities(@RequestParam(required = false) Long newerThan) {
        if(newerThan != null) {
            return mapper.entitiesToDtos(postEntityRepository.findByModifyDateGreaterThan(newerThan).orElse(new ArrayList<>()));
        }
        return mapper.entitiesToDtos(postEntityRepository.findAll());
    }
}
