package com.agrzybow.companyapp.backend.chatmanagement.model.repository;

import com.agrzybow.companyapp.backend.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChatMessageEntityRepository extends JpaRepository<ChatMessageEntity, Integer> {
    List<ChatMessageEntity> findByModifyBy(UserEntity modifyBy);
    List<ChatMessageEntity> findByUserEntityReceiver(UserEntity userEntityReceiver);
    List<ChatMessageEntity> findByUserEntityReceiverAndModifyDateGreaterThan(UserEntity userEntityReceiver, Long modifyDate);
    List<ChatMessageEntity> findByModifyByAndModifyDateGreaterThan(UserEntity modifyBy, Long modifyDate);
}
