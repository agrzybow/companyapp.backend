package com.agrzybow.companyapp.backend.general.model;

import com.agrzybow.companyapp.backend.general.util.UserEntitySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "UserPermission")
public class UserPermissionEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String MOBILE_APP_FIELD_NAME = "mobileApp";
    public static final String CONSOLE_DISPLAY_FIELD_NAME = "consoleDisplay";
    public static final String CONSOLE_EDIT_FIELD_NAME = "consoleEdit";
    public static final String CONSOLE_ADD_FIELD_NAME = "consoleAdd";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Column(name = MOBILE_APP_FIELD_NAME, nullable = false)
    private Boolean mobileApp;

    @NotNull
    @Column(name = CONSOLE_DISPLAY_FIELD_NAME, nullable = false)
    private Boolean consoleDisplay;

    @NotNull
    @Column(name = CONSOLE_EDIT_FIELD_NAME, nullable = false)
    private Boolean consoleEdit;

    @NotNull
    @Column(name = CONSOLE_ADD_FIELD_NAME, nullable = false)
    private Boolean consoleAdd;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MODIFY_BY_FIELD_NAME)
    @JsonSerialize(using = UserEntitySerializer.class)
    private UserEntity modifyBy;
}
