package com.agrzybow.companyapp.backend.general.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.repository.UserEntityRepository;
import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/user")
public class UserEntityAdminRestController implements AdminRestControllerInterface<UserEntity, String> {
    private UserEntityRepository userEntityRepository;

    @Autowired
    public UserEntityAdminRestController(UserEntityRepository userEntityRepository) {
        this.userEntityRepository = userEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<UserEntity> getAllEntities() {
       return userEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public UserEntity getEntityById(@PathVariable String id) {
        return userEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<UserEntity> createEntity(@RequestBody List<UserEntity> newEntities) {
        return userEntityRepository.saveAll(newEntities);
    }

    @PostMapping("/edit")
    @Override
    public UserEntity editEntity(@RequestBody UserEntity editedEntity) {
        return userEntityRepository.save(editedEntity);
    }
}
