package com.agrzybow.companyapp.backend.general.model.repository;

import com.agrzybow.companyapp.backend.general.model.UserPermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPermissionEntityRepository extends JpaRepository<UserPermissionEntity, Integer> {
}
