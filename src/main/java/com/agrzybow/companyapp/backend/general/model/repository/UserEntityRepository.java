package com.agrzybow.companyapp.backend.general.model.repository;

import com.agrzybow.companyapp.backend.general.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserEntityRepository extends JpaRepository<UserEntity, String> {
    Optional<List<UserEntity>> findByUsernameNotAndModifyDateGreaterThan(String username, Long newerThan);
    Optional<List<UserEntity>> findByUsernameNot(String username);
}
