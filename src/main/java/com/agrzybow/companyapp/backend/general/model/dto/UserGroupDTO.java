package com.agrzybow.companyapp.backend.general.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserGroupDTO {
    private Integer id;
    private String groupText;
    private Long modifyDate;
}
