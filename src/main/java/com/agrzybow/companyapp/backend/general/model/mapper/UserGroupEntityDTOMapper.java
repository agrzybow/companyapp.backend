package com.agrzybow.companyapp.backend.general.model.mapper;

import com.agrzybow.companyapp.backend.general.model.UserGroupEntity;
import com.agrzybow.companyapp.backend.general.model.dto.UserGroupDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserGroupEntityDTOMapper {
    UserGroupEntity dtoToEntity(UserGroupDTO userGroupDTO);
    UserGroupDTO entityToDto(UserGroupEntity userGroupEntity);

    List<UserGroupDTO> entitiesToDtos(List<UserGroupEntity> userGroupEntities);
    List<UserGroupEntity> dtosToEntities(List<UserGroupDTO> userGroupDTOs);
}
