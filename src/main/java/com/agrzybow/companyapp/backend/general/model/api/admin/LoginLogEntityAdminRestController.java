package com.agrzybow.companyapp.backend.general.model.api.admin;

import com.agrzybow.companyapp.backend.general.model.repository.LoginLogEntityRepository;
import com.agrzybow.companyapp.backend.general.model.LoginLogEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/login-log")
public class LoginLogEntityAdminRestController implements AdminRestControllerInterface<LoginLogEntity, Integer> {

    private LoginLogEntityRepository loginLogEntityRepository;

    @Autowired
    public LoginLogEntityAdminRestController(LoginLogEntityRepository loginLogEntityRepository) {
        this.loginLogEntityRepository = loginLogEntityRepository;
    }

    @GetMapping("/display")
    @Override
    public List<LoginLogEntity> getAllEntities() {
        return loginLogEntityRepository.findAll();
    }

    @GetMapping("/display/{id}")
    @Override
    public LoginLogEntity getEntityById(@PathVariable Integer id) {
        return loginLogEntityRepository.findById(id).orElse(null);
    }

    @PostMapping("/add")
    @Override
    public List<LoginLogEntity> createEntity(@RequestBody List<LoginLogEntity> newEntities) {
        return null;
    }

    @PostMapping("/edit")
    @Override
    public LoginLogEntity editEntity(@RequestBody LoginLogEntity editedEntity) {
        return null;
    }
}
